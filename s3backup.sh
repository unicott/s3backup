#!/bin/sh

###################
# Variables
###################

# Backup folder, the root is user folder where the script is running (~/)
s3bf="S3_Backup"
# Bucket name (also subfolder name to sync)
bucket="your_bucket"
# Zip/TarGz (set to false to use tar.gz compressor)
zip=true

# Status flag (don't change)
ok=false
# Date
today=`eval date +%Y%m%d_%H%M%S`
#today=`eval date +%d`

###################
# Main Logic
###################

# Check backup folder existence
if [ -d ~/$s3bf ]; then
  echo "Folder ~/$s3bf exists... OK."
  ok=true
else
  echo "Folder ~/$s3bf doesn't exists... \nMaking folder... "
  if mkdir -p ~/$s3bf ; then
    echo "OK."
    ok=true
  else
    echo "ERROR. Folder wasn't created..."
  fi
fi

# Check bucket folder existence 
if [ -d ~/$s3bf/$bucket ]; then
  echo "Folder ~/$s3bf/$bucket exists... OK."
else
  echo "Folder ~/$s3bf/$bucket doesn't exists... \nMaking folder..."
  if mkdir -p ~/$s3bf/$bucket ; then
    echo "OK."
  else
    echo "ERROR. Folder wasn't created..."
    ok=false
  fi
fi

# Start sync and read status of the sync
sync=true

if $ok; then
  echo "Sync..."
  if s3cmd sync s3://$bucket/ ~/$s3bf/$bucket/ ; then
    echo "\nOK."
  else 
    echo "\nFailure."
    sync=false
  fi
fi

# Check sync status 
zipped=true

if $sync; then
  echo "Compressing backup..."
  if $zip; then
    echo "Using zip ..."
    if zip -r ~/$s3bf/$bucket-$today.zip ~/$s3bf/$bucket ; then
      echo "OK."
    else
      echo "Failure."
      zipped=false
    fi
  else
    echo "Uzing tar.gz ..."
    if tar -czvf ~/$s3bf/$bucket-$today.tar.gz ~/$s3bf/$bucket ; then
      echo "OK."
    else  
      echo "Failure."
      zipped=false
    fi
  fi
fi


